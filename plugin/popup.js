var urlName = "";
var linkHash;

document.addEventListener('DOMContentLoaded', function () {

    getCurrentTabUrl(function (urlLink) {
        chrome.extension.getBackgroundPage().console.log(urlLink);
    });

    linkAdd.addEventListener("click", function () {
        chrome.extension.getBackgroundPage().console.log("Button gedrückt");
        chrome.tabs.create({url: "http://linkfinder.abas.tools/add/search?param=" + urlName});
    });

    linkInfo.addEventListener("click", function () {
        chrome.extension.getBackgroundPage().console.log("Button gedrückt");
        chrome.tabs.create({url: "http://linkfinderserver.abas.tools:10010/key/show/" + linkHash});
    });
});
