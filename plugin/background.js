function getCurrentTabUrl(callback) {

    var queryInfo = {
        active: true,
        currentWindow: true
    };
    chrome.tabs.query(queryInfo, function (tabs) {
        var tab = tabs[0];
        var urlLink = tab.url;
        console.assert(typeof urlLink == 'string', 'tab.url should be a string');
        urlName = urlLink;
        var smallUrlName = urlLink.toLowerCase();
        var hash = md5(smallUrlName);
        chrome.extension.getBackgroundPage().console.log(hash);
        linkHash = hash;

        var xmlHttp = new XMLHttpRequest();
        var request = "http://linkfinderserver.abas.tools:10010/links/key/show/"+hash;
        xmlHttp.open( "GET", request, false);
        xmlHttp.send();

        if (xmlHttp.status == 404) {
            document.getElementById('linkInfo').style.display="none";
            document.getElementById('linkAdd').innerHTML = "Link speichern!";
        }
        else {
            document.getElementById('linkAdd').style.display="none";
            document.getElementById('linkInfo').style.background = "#E40046";
            document.getElementById('linkInfo').innerHTML = "Linkinfos anzeigen!";
        }
        callback(urlLink);
    });
}