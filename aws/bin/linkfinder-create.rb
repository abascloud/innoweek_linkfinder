#!/usr/bin/ruby

PROJECT_DIR = File.expand_path("../..", __FILE__)
CFN_TEMPLATE = File.expand_path("cloudformation/link-finder.yaml", PROJECT_DIR)
DEFAULT_STACK = "abas-linkfinder-dev-stack"
DEFAULT_REGION = "eu-west-1"
DEFAULT_STAGE = "dev"
DEFAULT_PROFILE = "bau"

print "Stack (#{DEFAULT_STACK}): "
stack = gets.chomp
stack = DEFAULT_STACK if stack.empty?

print "Region (#{DEFAULT_REGION}): "
region = gets.chomp
region = DEFAULT_REGION if region.empty?

print "Stage (#{DEFAULT_STAGE}): "
stage = gets.chomp
stage = DEFAULT_STAGE if stage.empty?

cmd = "aws --profile #{DEFAULT_PROFILE} --region #{region} cloudformation create-stack --stack-name #{stack} --template-body file://#{CFN_TEMPLATE} --capabilities CAPABILITY_NAMED_IAM"

unless stage.empty?
  cmd += " --parameters"
end

unless region.empty?
  cmd += " ParameterKey=Stage,ParameterValue=#{stage}"
end

puts cmd
system (cmd)
