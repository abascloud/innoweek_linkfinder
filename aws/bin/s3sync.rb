#!/usr/bin/ruby

PROJECT_DIR = File.expand_path("../..", __FILE__)
S3_RESOURCE_DIR = File.expand_path("../app/build/unbundled", PROJECT_DIR)
DEFAULT_BUCKET = "dev-linkfinder.abas.tools"
DEFAULT_PROFILE = "bau"

print "Bucket (#{DEFAULT_BUCKET}): "
bucket = gets.chomp
bucket = DEFAULT_BUCKET if bucket.empty?

cmd = "aws --profile #{DEFAULT_PROFILE} s3 sync #{S3_RESOURCE_DIR} s3://#{bucket} --delete"
puts cmd
system (cmd)


