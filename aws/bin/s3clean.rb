#!/usr/bin/ruby

PROJECT_DIR = File.expand_path("../..", __FILE__)
S3_RESOURCE_DIR = File.expand_path("s3-resources", PROJECT_DIR)
DEFAULT_BUCKET = "dev-linkfinder.abas.tools"
DEFAULT_PROFILE = "bau"

print "Bucket (#{DEFAULT_BUCKET}): "
bucket = gets.chomp
bucket = DEFAULT_BUCKET if bucket.empty?

cmd = "aws --profile #{DEFAULT_PROFILE} s3 rm s3://#{bucket} --recursive"
puts cmd

puts "If you really want to delete all files uncomment the line 'system (cmd)'"
#system (cmd)


