'use strict';

//import LinkPreview from 'react-native-react-native-link-preview'; 

const Link = require('../model/link');
const Tag = require('../model/tag');
const Suggestion = require('../model/suggestion');
const preview = require('page-previewer');

var util = require('util');
var db_manager = require('../../config/db_arango');

module.exports = {
  dbinit: dbinit,
  addLink: addLink,
  getLink: getLink,
  getLinkPreview: getLinkPreview,
  listLinks: listLinks,
  filterLinks: filterLinks
};

function dbinit(req, res) {
  console.log("I'm working!");
  db_manager.init();
  res.json({success: 1, description: "ok"});
}

function addLink(req, res) {
  console.log("addLink");
  console.log(JSON.stringify(req.body));

  db_manager.saveLink(req.body.url, req.body.id, req.body.tags)
  .then(function(result) {
        console.log(result);
        res.status(201);
        res.json({success: 1, description: "created"});
      })
  .catch(function(err) {
        console.error(err);
        res.status(400);
        res.json({message: "Something went terrible wrong!"});
      });
}

function getLink(req, res) {
  console.log("getLink");

  db_manager.getLink(req.swagger.params.id.value)
  .then(function(result) {
        console.log(JSON.stringify(result));
        res.status(200);
        res.json(JSON.stringify(result));
      })
  .catch(function(err) {
        console.error(err);
        res.status(404);
        res.json({message: "Not Found!"});
      });
}

function getLinkPreview(req, res) {
  console.log("getLinkPreview");

  db_manager.getLink(req.swagger.params.id.value)
  .then(function(result) {

  	preview({url: "https://abas-erp.com/", proxy: "http://webproxy.abas.de:8000"} , function(err, data) {
  		if(!err) {
        console.log(JSON.stringify(data));
        res.status(200);
        res.json(JSON.stringify(result));
  		} else {
        console.error(err);
        res.status(403);
        res.json({message: "Could not fetch preview!"});
  		}
  	});
	}).catch(function(err) {
        console.error(err);
        res.status(404);
        res.json({message: "Not Found!"});
      });
}

function listLinks(req, res) {
  console.log("listLinks");

  db_manager.listLinks()
  .then(function(result) {
        console.log(JSON.stringify(result));
        res.status(200);
        res.json(JSON.stringify(result));
      })
  .catch(function(err) {
        console.error(err);
        res.status(403);
        res.json({message: "Not Found!"});
      });
}

function filterLinks(req, res) {
  let tagStringArray = req.swagger.params.searchtags.value.replace(/ /g,"").replace(/"/g,"").split(",");
  console.log("filterLinks with tags: " + tagStringArray);

	db_manager.filterLinks(tagStringArray)
		.then(function(result) {
				console.log(JSON.stringify(result));
				res.status(200);
				res.json(JSON.stringify(result));
			}).catch(function(err) {
				console.error(err);
				res.status(500);
				res.json({message: "Not Found!"});
			});
}
