'use strict';

const Link = require('../model/link');
const Tag = require('../model/tag');
const Suggestion = require('../model/suggestion');

var util = require('util');
var db_manager = require('../../config/db_arango');

module.exports = {
  addTag: addTag,
  filterTags: filterTags,
  getTags: getTags
};

function addTag(req, res) {
  console.log("addTag");

  db_manager.createAndGetTag(req.body.title)
  .then(function(result) {
        console.log(result);
        res.status(201);
        res.json({success: 1, description: "created"});
      })
  .catch(function(err) {
        console.error(err);
        res.status(400);
        res.json({message: "Something went terrible wrong!"});
      });
}

function getTags(req, res) {
  console.log("getTags");

  db_manager.listTags()
  .then(function(result) {
        console.log(result);
        res.status(201);
        res.json(result);
      })
  .catch(function(err) {
        console.error(err);
        res.status(400);
        res.json({message: "Something went terrible wrong!"});
      });
}

function filterTags(req, res) {
  let tagStringArray = req.swagger.params.searchtags.value.replace(/ /g,"").replace(/"/g,"").split(",");
  console.log("filterTags with tags: " + tagStringArray);

	if (tagStringArray === undefined || tagStringArray == null || tagStringArray.length == 0 || tagStringArray[0] == "") {
		getTags(req, res);
	} else {
		db_manager.filterTags(tagStringArray)
			.then(function(result) {
					console.log(JSON.stringify(result));
					res.status(200);
					res.json(result);
				}).catch(function(err) {
					console.error(err);
					res.status(500);
					res.json({message: "Not Found!"});
				});
	}

}
