'use strict';

const Link = require('../model/link');
const Tag = require('../model/tag');
const Suggestion = require('../model/suggestion');

var util = require('util');
var db_manager = require('../../config/db_arango');

module.exports = {
	listSuggestions: listSuggestions
};

function listSuggestions(req, res) {
  console.log("getSuggestions");

  db_manager.listSuggestions()
  .then(function(result) {
        console.log(result);
        res.status(201);
        res.json(result);
      })
  .catch(function(err) {
        console.error(err);
        res.status(400);
        res.json({message: "Something went terrible wrong!"});
      });
}
