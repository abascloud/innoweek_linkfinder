'use strict'

class Suggestion {
  constructor(title) {
    this._title = title;
  }

  verify() {
    return new Promise(function(resolve, reject) {
      console.log("Verify: ", this._title );
      return (this._title.length > 2);
    });
  }
}

module.exports = Suggestion;
