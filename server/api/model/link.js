'use strict'

class Link {

  constructor(url, id) {
    this.url = url;
    this.id = id;
  }

  verify() {
    console.log("Verify: ", this.url );
    return (this.url.length > 2);
  }
}

module.exports = Link;
