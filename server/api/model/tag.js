'use strict'

class Tag {
  constructor(title) {
    this.title = title;
  }

  verify() {
    return new Promise(function(resolve, reject) {
      console.log("Verify: ", this.title );
      return (this.title.length > 2);
    });
  }
}

module.exports = Tag;
