'use strict'

const Link = require('../api/model/link.js');

let host = "arango";
//let host = "localhost";
let port = 8529;
let username = "root";
let password = "1234";


var Database = require('arangojs').Database;
var aql = require('arangojs').aql;

let db_arango = new Database({
    url: `http://${username}:${password}@${host}:${port}`
});
let db_finder = new Database({
    url: `http://${username}:${password}@${host}:${port}`,
    databaseName: `finderdb`
});

let link_collection = db_finder.collection('links');
let tag_collection = db_finder.collection('tags');
let link_tag_collection = db_finder.edgeCollection('isTagged');

let tag_graph = db_finder.graph('tagged');

function dbinit()  {
  link_collection.create().then(
    () => console.log('Link Collection created'),
    err => console.log('Failed to create link collection: ', err)
  );

  tag_collection.create().then(
    () => console.log('Tag Collection created'),
    err => console.log('Failed to create tag collection: ', err)
  );

  link_tag_collection.create({ waitForSync: true }).then(
    () => console.log('Link-Tag Collection created'),
    err => console.log('Failed to create link-tag collection: ', err)
  );

  tag_graph.create({
		edgeDefinitions: [
			{
				collection: 'isTagged',
				from: [
					'tags'
				],
				to: [
					'links'
				]
			}
		]
  }).then(x => {
    	console.log('Tag Graph created');
			testData();
    }).catch(err => {
  		console.log('Failed to create tag graph: ', err);
  	});
}

function testData() {
	let data = [
		{url: "http://jenkins.abasag.intra/", id: "fb2d87ff6a052a31c5522a6c66a5fbea", tags: ["abas", "jenkins", "abasag.intra", "hudson", "abas-mitarbeiter-links"]},
		{url: "https://bitbucket.org/abascloud/", id: "d97da4aca9c95511e9994fa11fe5b286", tags: ["bitbucket", "abas", "abas-cloud", "abas-mitarbeiter-links"]},
		{url: "https://abascloud.atlassian.net/secure/rapidboard.jspa?rapidview=13&view=planning", id: "f12161f6b07d90ab3e38fad2e82fb22c", tags: ["jira", "backlog", "workflow"]},
		{url: "https://abascloud.atlassian.net/wiki/display/wor/reviews", id: "473bfc50d656c7fa50720c08125fc8b0", tags: ["confluence", "workflow", "review"]},
		{url: "https://workflow.jenkins.abas.sh/login?from=%2fjob%2fworkflow.multibranch%2fjob%2fmaster%2f", id: "473bfc50d656c7fa50720c08125fc8b0", tags: ["jenkins", "workflow", "master"]},
		{url: "http://ardapedia.abasag.intra/webclient/branch/master/client/currentshowcase/showcase.html#showcase;widget=webfont", id: "536d139945ac42b339c451e458242f9a", tags: ["abas", "abasag.intra", "webfonts"]},
		{url: "http://extranet.abas.de/sub_de/index.php", id: "081ad9f9bf0e31db84a9353135527916", tags: ["extranet", "abas", "abas-mitarbeiter-links"]},
		{url: "https://goldrush.abasag.intra/mportal/", id: "157b27d254629db4be964dca2afbe226", tags: ["abas", "abasag.intra", "mitarbeiterportal", "urlaub", "abas-mitarbeiter-links"]},
		{url: "https://cntcustomers2.cnt.net/login.html", id: "c19d50f3fec1463b5b565e601d3ad04f", tags: ["cnt", "call-manager", "abas", "abas-mitarbeiter-links"]},
		{url: "https://rogerdudler.github.io/git-guide/index.de.html", id: "50294040eb700d98aaaca4f725121ad2", tags: ["git", "befehle", "uebersicht"]},
		{url: "https://docs.camunda.org/manual/7.6/", id: "b72ac95ece9961abb6755caf276bfe4f", tags: ["camunda", "dokumentation"]},
		{url: "http://corporate.ef.com/partner/corp/", id: "ae40939c50ce442ed702ca321eb75a46", tags: ["englisch-kurs", "abas", "abas-mitarbeiter-links"]},
		{url: "https://wiki.abasag.intra/pages/viewpage.action?pageId=56854339", id: "452cd80467d28799b43ba7ebeab41860", tags: ["abas", "abasag.intra", "wiki","drucker", "hinzufuegen", "abas-mitarbeiter-links"]},
		{url: "https://wiki.abasag.intra/pages/viewpage.action?pageId=50758309", id: "a6d9f7e135120c52caf02fd621233969", tags: ["fuehrungsteam", "entwicklung", "protokoll", "abas-mitarbeiter-links"]},

		{url: "https://abascloud.atlassian.net/wiki/discover/all-updates", id: "43a8adf3d8b065fee9b16bd771fc4c61", tags: ["confluence", "startseite", "abas-mitarbeiter-links"]},
		{url: "https://elements.polymer-project.org/", id: "50673322233c1d49e3bf2fcf3a136b93", tags: ["polymer", "elemets", "catalog", "overview", "2.0"]},
		{url: "https://www.polymer-project.org/1.0/docs/tools/tests", id: "735d18029cb2526495476360c84f16fb", tags: ["polymer", "overview", "1.0"]},
		{url: "https://www.youtube.com/watch?v=hgJ0Xcybwzy", id: "7a24d561cf00a6e52d1c84bdbe75b505", tags: ["youtube", "polymer", "starter-kit-2", "tutorial"]},
		{url: "https://www.youtube.com/watch?v=kX2inpjy4y4", id: "b235a49cb24e443d220bcab09798633d", tags: ["youtube", "polymer", "testing", "tutorial","web-components"]},
		{url: "https://www.youtube.com/watch?v=zdjiumx51y8&t=784s", id: "4c2e472b93be0efcfadd47f8fc99f891", tags: ["youtube", "polymer", "overview"]},
		{url: "https://www.youtube.com/watch?v=1sx6ynn58oq", id: "afc79f6ccbcf559e6874a1d2e8f7c56d", tags: ["youtube", "polymer", "data-binding"]},
		{url: "https://www.w3schools.com/", id: "d9da49789ed2cd0935e76a08fdc0319f", tags: ["w3schools", "html", "css","java-script"]},
		{url: "https://wiki.abasag.intra/display/scrum/innovation+week+2017+may", id: "99879110e94773600face7491b622fd6", tags: ["wiki", "abasag.intra", "innovation-week","2017"]},
		{url: "https://wiki.abasag.intra/display/scrum/link+finder", id: "4a8c9745ba8fe1d0266fcc1f329c9e4d", tags: ["wiki", "abasag.intra", "innovation-week", "link-finder"]},

		{url: "https://www.webcomponents.org/element/polymerelements/paper-card/demo/demo/index.html", id: " d59b2c618a935890bf272bfdab1dd0f0", tags: ["polymer", "paper-card", "webcomponents"]},
		{url: "https://www.youtube.com/playlist?list=pllnphn493bhghogab2prkzv4zw3qoatk-", id: " cfa2f074e637d08fcca286d4c4be22b5", tags: ["polymer", "tutorial", "youtube","video"]},
		{url: "http://blog.raffaeu.com/archive/2016/02/17/get-started-with-polymer-1-0-and-webstorm-11.aspx", id: "cfa2f074e637d08fcca286d4c4be22b5", tags: ["polymer", "webstorm", "getting-started","tutorial","manual","blog"]},
		{url: "https://blog.selfhtml.org/2014/12/09/web-components-eine-einfuehrung/", id: "38163bb14c949b6f22858e056075d616", tags: ["polymer", "webcomponents", "einfuehrung","ueberblick","blog","selfhtml"]},
		{url: "http://bumbum.abasag.intra:8111/", id: "18aa6890eabb4271cf45ffa2c0aaebcf", tags: ["teamcity", "build", "server","bapps","abasag.intra"]},
		{url: "http://qmsystem.abasag.intra", id: " 16a40a2f50526f05228f3b760ed8da49 ", tags: ["qm", "qualitaetsmanagement", "organigramme","abasag.intra", "abas-mitarbeiter-links"]},
		{url: "https://discuss.service.abas/", id: "efb1807483088d730574ad4ac44daa37", tags: ["diskussionsforum", "discuss", "forum","hilfe","diverses","abas"]},
		{url: "http://search.abas.de:12580/deploy/diagsearch.html", id: "c7919eee583ca70ab5ff1977c6addaa0", tags: ["diags", "suche", "diagnose","meldungen","anfragen","support","abas"]},
		{url: "https://www.youtube.com/watch?v=jgsjgxiusg", id: " 745b11432d419f5e1e9338a4edfbfb49 ", tags: ["bitbucket", "video", "youtube","einrichtung","repository"]},

		{url: "https://jira.abasag.intra/secure/Dashboard.jspa", id: "b5832e45a497d94106ff86a5fbccbda8", tags: ["jira", "tickets", "abasag.intra", "issues", "dashboard", "abas-mitarbeiter-links"]},
		{url: "https://eu-central-1.signin.aws.amazon.com", id: "https://jira.abasag.intra/secure/dashboard.jspa", tags: ["amazon", "aws", "login", "cloud", "console"]},
		{url: "https://wiki.abasag.intra/#all-updates", id: "b003c7f50ec834939efc49b3cbf4a199", tags: ["wiki", "abasag.intra", "abas", "abas-mitarbeiter-links"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-reconfigure/", id: "7bd1a8a6f0e899c3527f2650c327209b", tags: ["jenkinsjob", "jenkins", "vagrant", "demosystem", "vmware", "vm-belka-vgr3", "vm-gordo-vgr5", "reconfigure", "v2017r2"]},
		{url: "http://jenkins.abasag.intra/job/vm-v2017r2/", id: "fbbabe368e64f27ad0bdfdd8c7462bcb", tags: ["jenkinsjob", "jenkins", "demosystem", "vmware", "vagrant", "vm-belka-vgr3", "v2017r2"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-mw/", id: "4f822b243c687b1b7a676e2f547955fd", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "mw"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-mw-test/", id: "461b98f3441666590cde34bc287063e5", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "mw", "test"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-aba/", id: "3e485a255045dc2773220ec914567606", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "aba", "bapps", "business-apps"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-configurator/", id: "3e485a255045dc2773220ec914567606", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "configurator"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-icedemon/", id: "e83631a47dd6036678d03398c282fcdc", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "icedemon"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-mobile/", id: "e83631a47dd6036678d03398c282fcdc", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "mobile"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-rabbitmq/", id: "e29f00a015b9ce2998f5e91f588c8534", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "rabbitmq"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-unity/", id: "e29f00a015b9ce2998f5e91f588c8534", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "unity"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-usa-bapps/", id: "e29f00a015b9ce2998f5e91f588c8534", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "usa", "bapps", "business-apps"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-usa-workflow/", id: "e29f00a015b9ce2998f5e91f588c8534", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "usa", "workflow"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-vendor-portal/", id: "6df7e686defee3e2cd5cb26d7f8d9f7d", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "vendor-portal", "portal"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-webclient/", id: "0dae8e5b46eb71c5c1fe1fe91f4069a6", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "webclient"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-webshop/", id: "56882a3a82a85365f1bec26255506e67", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "webshop"]},
		{url: "http://jenkins.abasag.intra/job/abas-vagrant-vm-def-workflow/", id: "8bf15d8cc6bb9213ac326294b80bc142", tags: ["jenkinsjob", "jenkins", "demosystem", "vagrant", "workflow"]},

		{url: "http://extranet.abas.de/sub_de/service-support/mitarbeiter-academy/ausbildungsprogramm.php", id: "b634b336c9f9c9f1137b2fce2af0c817", tags: ["extranet", "ausbildungsprogramm", "training", "schulung", "abas-mitarbeiter-links"]},
	];

	data.forEach(x => {
		saveLink(x.url, x.id, x.tags);
	});

}

module.exports = {
  init: init,
  saveLink: saveLink,
  createAndGetTag: createAndGetTag,
  listTags: listTags,
  getLink: getLink,
  listLinks: listLinks,
  listSuggestions: listSuggestions,
  filterLinks: filterLinks,
  filterTags: filterTags,
  getTags: getTags
};


function init() {
	db_arango.createDatabase('finderdb').then(
		() => dbinit(),
		err => console.log('Failed to create database: ', err)
	);
}

function saveLink(url, id, tags) {
	return new Promise(function(resolve, reject) {
		link_collection.save({url: url, _key: id, title: url})
		.then(result => {
				console.log('Document saved:', result._rev);
				let tag_promises = tags.map(createAndGetTag);
				Promise.all(tag_promises)
					.then(values =>  {
						console.log(values);
						createEdges(result, values)
							.then(values2 => {
								console.log(values2);
								resolve(values2);
							}).catch(err => {
								console.error('Failed to save link:', err);
								reject(Error("Could not save link (3)"));
							});
					}).catch(err => {
						console.error('Failed to save link:', err);
						reject(Error("Could not save link (2)"));
					});

			})
		.catch(err => {
				console.error('Failed to save document:', err);
				reject(Error("Could not save link"));
			}
		);
	});
}

function createEdges(link, tags) {
	return new Promise(function(resolve, reject) {
		let edge_promises = tags.map(tag => link_tag_collection.save({}, tag, link));
		Promise.all(edge_promises)
			.then(values => {
				console.log(values);
				resolve(values);
			}).catch(err => {
				console.error(err);
				reject(err);
			});
	});
}

function createAndGetTag(tag) {
	return new Promise(function(resolve, reject) {
		console.log("HERE: " + tag);
		tag_collection.save({_key: tag, title: tag })
		.then(result => {
				console.log('Document saved:', result._rev);
				getTag(tag)
					.then(result2 => resolve(result2))
					.catch(err => reject(Error("Could not find existing tag")));
			})
		.catch(err => {
				getTag(tag)
					.then(result2 => resolve(result2))
					.catch(err => reject(Error("Could not find existing tag")));
				console.log('Tag exists already');
			}
		);
	});
}

function getTag(title) {
  return tag_collection.document(title);
}

function getTags(tagStringArray) {
  return new Promise(function(resolve, reject) {
    let tag_promises = tagStringArray.map(getTag);
    Promise.all(tag_promises)
      .then(values => {
        console.log(values);
        resolve(values);
      }).catch(err => {
        console.error(err);
        reject(err);
      });
  });
}

function listSuggestions() {
  return new Promise(function(resolve, reject) {
		console.log("listSuggestions");

		let aql_query =`
			LET c = (FOR x IN isTagged COLLECT tag = x._from WITH COUNT INTO counter RETURN {tag, counter})
			FOR tag IN c
				RETURN MERGE(DOCUMENT(tag.tag), {count: tag.counter})
			`;

		console.log("AQL: " + aql_query);

    db_finder.query(aql_query)
      .then(cursor => cursor.all())
      .then(result => {
        console.log('Found link: ', result);
        if (result === undefined || result == null) {
            reject(Error("Link not found"));
        } else {
          resolve(result);
        }
      }).catch(err => {
        console.error('Failed to fetch all documents:', err);
        reject(Error("Could not retrieve link"));
      });
	});
}

function listTags() {
	return new Promise(function(resolve, reject) {
		tag_collection.all().then(
			cursor => cursor.all()
		).then(tags => {
				resolve(tags);
			}
		).catch(err => {
				console.error('Failed to fetch all documents:', err);
			}
		);
	});
}

function getLink(key) {
	return new Promise(function(resolve, reject) {
		let id = "links/" + key
    db_finder.query(aql`
      FOR link IN links
          FILTER link._key == ${key}
          RETURN {
              "link": link,
              "tags": (
                  FOR tag IN ANY ${id} isTagged OPTIONS {bfs: true, uniqueVertices: 'global'} RETURN tag
              )
          }
      `)
      .then(cursor => cursor.all())
      .then(result => {
        console.log('Found link: ', result);
        if (result === undefined || result == null || result.length == 0) {
            reject(Error("Link not found"));
        } else {
          resolve(result[0]);
        }
        })
      .catch(err => {
          console.error('Failed to fetch all documents:', err);
          reject(Error("Could not retrieve link"));
        });
	});
}

function listLinks() {
  return new Promise(function(resolve, reject) {
  	console.log("listLinks");
    db_finder.query(aql`
      FOR link IN links
          RETURN {
              "link": link,
              "tags": (
                  FOR tag IN INBOUND link._id isTagged OPTIONS {bfs: true, uniqueVertices: 'global'} RETURN tag
              )
          }
      `)
      .then(cursor => cursor.all())
      .then(result => {
        console.log('Found link: ', result);
        if (result === undefined || result == null || result.length == 0) {
            reject(Error("Link not found"));
        } else {
          resolve(result);
        }
        })
      .catch(err => {
          console.error('Failed to fetch all documents:', err);
          reject(Error("Could not retrieve link"));
        });
	});
}

function filterLinks(tags) {
  return new Promise(function(resolve, reject) {
		let tag_string = JSON.stringify(tags);
		console.log("tag_string: " + tag_string);

		let aql_query =`
			FOR link IN links
    		LET tags = (
        	FOR x IN INBOUND link isTagged OPTIONS {bfs: true, uniqueVertices: 'global'} RETURN x._key
    		)
    		FILTER ${tag_string} ALL IN tags
    		RETURN MERGE(link, {tags})
			`;

		console.log("AQL: " + aql_query);

    db_finder.query(aql_query)
      .then(cursor => cursor.all())
      .then(result => {
        console.log('Found link: ', result);
        if (result === undefined || result == null) {
            reject(Error("Link not found"));
        } else {
          resolve(result);
        }
      }).catch(err => {
        console.error('Failed to fetch all documents:', err);
        reject(Error("Could not retrieve link"));
      });
	});
}

function filterTags(tags) {
  return new Promise(function(resolve, reject) {
		let tag_string = JSON.stringify(tags);
		console.log("tag_string: " + tag_string);

		let aql_query =`
			LET links = (
    		FOR x IN ${tag_string}
        	COLLECT l = (
            FOR link IN OUTBOUND CONCAT('tags/', x) isTagged OPTIONS {bfs: true, uniqueVertices: 'global'} RETURN DISTINCT link
        	) RETURN l
			)

			LET distinct_links = APPLY("INTERSECTION", links)

			LET results = (
    		FOR link IN distinct_links
        	COLLECT tags = (
            FOR tag IN INBOUND link isTagged OPTIONS {bfs: true, uniqueVertices: 'global'} RETURN tag
        	) RETURN tags
			)

			RETURN APPLY("UNION_DISTINCT", results)	
		`;

		console.log("AQL: " + aql_query);

    db_finder.query(aql_query)
      .then(cursor => cursor.all())
      .then(result => {
        console.log('Found link: ', result);
        if (result === undefined || result == null || result.length == 0) {
            reject(Error("Link not found"));
        } else {
        	console.log("YAY: " + result);
          resolve(result[0]);
        }
      }).catch(err => {
        console.error('Failed to fetch all documents:', err);
        reject(Error("Could not retrieve link"));
      });
	});
}
